#include <FastLED.h> //Kütüphane
#define NUM_LEDS 16 //Toplam LED Sayısı
#define DATA_PIN 6 //Data Pini
const int buton = 7;
int butondurumu = 0;
CRGB leds[NUM_LEDS];
int deger=0;
void setup()
{
  
  pinMode(buton,INPUT);
  
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop()
{
  
 butondurumu = digitalRead(buton); 
  
    if ( (butondurumu == HIGH) && (deger == 0) ) {
  
  leds[0] = CRGB::Red; //kırmızı
  leds[1] = CRGB::Red;
  leds[2] = CRGB::Red;
  leds[3] = CRGB::Red;
  leds[4] = CRGB::Red;
  leds[5] = CRGB::Red;
  leds[6] = CRGB::Red;
  leds[7] = CRGB::Red;
  leds[8] = CRGB::Red; 
  leds[9] = CRGB::Red;
  leds[10] = CRGB::Red;
  leds[11] = CRGB::Red;
  leds[12] = CRGB::Red;
  leds[13] = CRGB::Red;
  leds[14] = CRGB::Red;
  leds[15] = CRGB::Red;
  deger=1;
  FastLED.show();
  delay(400);
    }
  
  butondurumu = digitalRead(buton);
  
  
  
    if ( (butondurumu == HIGH) && (deger == 1) ) {
  leds[0] = CRGB::Blue; //Mavi
  leds[1] = CRGB::Blue;
  leds[2] = CRGB::Blue;
  leds[3] = CRGB::Blue;
  leds[4] = CRGB::Blue;
  leds[5] = CRGB::Blue;
  leds[6] = CRGB::Blue;
  leds[7] = CRGB::Blue;
  leds[8] = CRGB::Blue; //Mavi
  leds[9] = CRGB::Blue;
  leds[10] =CRGB::Blue;
  leds[11] =CRGB::Blue;
  leds[12] =CRGB::Blue;
  leds[13] = CRGB::Blue;
  leds[14] = CRGB::Blue;
  leds[15] = CRGB::Blue;
  deger=2;
  FastLED.show();
  delay(400);
 
  
  }
  
   butondurumu = digitalRead(buton);

  
  
    if ( (butondurumu == HIGH) && (deger == 2) ) {
  
  leds[0] = CRGB::Green; //Yeşil
  leds[1] = CRGB::Green;
  leds[2] = CRGB::Green;
  leds[3] = CRGB::Green;
  leds[4] = CRGB::Green;
  leds[5] = CRGB::Green;
  leds[6] = CRGB::Green;
  leds[7] = CRGB::Green;
  leds[8] = CRGB::Green; //Yeşil
  leds[9] = CRGB::Green;
  leds[10] = CRGB::Green;
  leds[11] = CRGB::Green;
  leds[12] = CRGB::Green;
  leds[13] = CRGB::Green;
  leds[14] = CRGB::Green;
  leds[15] = CRGB::Green;
  deger=3;
  delay(400);
  FastLED.show();
  }
  
    butondurumu = digitalRead(buton);

  
  
    if ( (butondurumu == HIGH) && (deger == 3) ) {
  
  leds[0] = CRGB::White; //Yeşil
  leds[1] = CRGB::White;
  leds[2] = CRGB::White;
  leds[3] = CRGB::White;
  leds[4] = CRGB::White;
  leds[5] = CRGB::White;
  leds[6] = CRGB::White;
  leds[7] = CRGB::White;
  leds[8] = CRGB::White; //Yeşil
  leds[9] = CRGB::White;
  leds[10] = CRGB::White;
  leds[11] = CRGB::White;
  leds[12] = CRGB::White;
  leds[13] = CRGB::White;
  leds[14] = CRGB::White;
  leds[15] = CRGB::White;
  deger=0;
  delay(400);
  FastLED.show();
  }
  
  
 
  
 
}
