from flask import Flask, jsonify, request
import base64
import json
import light as system

app = Flask(__name__)


@app.route("/")
def index():
    return "Tarbot Multispectral Camera Design"


@app.route("/port/<string:stat>", methods=["POST"])
def status(stat):
    return system.status(stat)


@app.route("/capture/path/<string:path>", methods=["POST"])
def DIRpath(path):
    return system.localPath(path)


@app.route("/capture", methods=["POST"])
def capture():
    data = request.get_json()
    state = system.selector(data)
    return state


if __name__ == "__main__":
    app.run(debug=True)
