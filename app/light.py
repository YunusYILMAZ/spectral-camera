# Importing Libraries
import serial
import time
import sys
import glob
import serial
import threading
from pyueyeCam import Ueyecam
from cameraUI import CameraUI

camera_ui = CameraUI()
cam = Ueyecam()

arduino = serial.Serial()


def status(_stat):
    if _stat == "open":
        portOpen()
        cam.start()
    elif _stat == "close":
        portClose()
        cam.stop()
    else:
        print("404")

    return _stat


def portOpen():
    arduino.baudrate = 9600
    arduino.port = "COM7"
    arduino.open()


def selector(data):
    for color in data.keys():
        arduino.write((color + data[color] + "\n").encode())
        time.sleep(0.4)

        frame = cam.shutter()
        # rgb_image = camera_ui.convert_to_rgb(frame, color)
        camera_ui.save_img(color + data[color], frame)
        time.sleep(0.8)
        arduino.write((color + "0\n").encode())
        time.sleep(0.2)
    return "return_data"


def portClose():
    arduino.close()


def portCheck():
    return arduino.is_open


def localPath(path):
    camera_ui.create_file(path)


# while True:
#     num = input("Enter a number: ")  # Taking input from user
#     value = write_read(num)
#     print(value)  # printing the value
