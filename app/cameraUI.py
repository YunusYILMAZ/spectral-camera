import numpy as np
import cv2
import os
from datetime import datetime


class CameraUI:
    def __init__(self):
        now = datetime.now()
        timeArray = str(now.strftime("%d:%m:%Y")).split(":")
        directory_ = timeArray[0] + "." + timeArray[1] + "." + timeArray[2]
        self.RGB_Image = np.zeros((1024, 1024, 3))
        self.msg = ""
        parent_dir = "C:/Users/yilma/Desktop/TARBOOT/spectral-camera/app/samples"
        self.path = os.path.join(parent_dir, directory_)
        self.local_path = os.path.join(self.path, "directory_")
        try:
            os.mkdir(self.path)
            print("Create Directory")
        except:
            print("Cannot create a file when that file already exists: ", directory_)

    def convert_to_rgb(self, frame, color):
        print("captured")
        global msg
        if list(color)[0] == "r":
            for i in range(1024):
                for j in range(1024):
                    self.RGB_Image[i][j][2] = frame[i][j]
        elif list(color)[0] == "i":
            for i in range(1024):
                for j in range(1024):
                    self.RGB_Image[i][j][2] = frame[i][j]
        elif list(color)[0] == "g":
            for i in range(1024):
                for j in range(1024):
                    self.RGB_Image[i][j][1] = frame[i][j]
        elif list(color)[0] == "b":
            for i in range(1024):
                for j in range(1024):
                    self.RGB_Image[i][j][0] = frame[i][j]
        return self.RGB_Image

    def save_img(self, name, frame):
        cv2.imwrite(self.local_path + "/" + name + ".jpg", frame)

    def create_file(self, name):
        now = datetime.now()
        timeArray = str(now.strftime("%H:%M:%S")).split(":")
        self.local_path = os.path.join(
            self.path, (name + timeArray[0] + timeArray[1] + timeArray[2])
        )
        os.mkdir(self.local_path)
