/*
 * 14 Royal Blue
 * 15 Amber
 * 16 IR
 * 17 White
 * 18 UV
 * 19 Red
 * 12 Blue
 * 13 Green
*/

/*
 * PIN variable
*/
#define RoyalBlue 14
#define Amber 15
#define IR 16
#define White 17
#define UV 18
#define Red 19
#define Blue 12
#define Green 13

#include <SoftPWM.h>

SOFTPWM_DEFINE_CHANNEL(0, DDRD, PORTD, PORTD0);  //Arduino pin 0
SOFTPWM_DEFINE_CHANNEL(1, DDRD, PORTD, PORTD1);  //Arduino pin 1
SOFTPWM_DEFINE_CHANNEL(2, DDRD, PORTD, PORTD2);  //Arduino pin 2
SOFTPWM_DEFINE_CHANNEL(3, DDRD, PORTD, PORTD3);  //Arduino pin 3
SOFTPWM_DEFINE_CHANNEL(4, DDRD, PORTD, PORTD4);  //Arduino pin 4
SOFTPWM_DEFINE_CHANNEL(5, DDRD, PORTD, PORTD5);  //Arduino pin 5
SOFTPWM_DEFINE_CHANNEL(6, DDRD, PORTD, PORTD6);  //Arduino pin 6
SOFTPWM_DEFINE_CHANNEL(7, DDRD, PORTD, PORTD7);  //Arduino pin 7
SOFTPWM_DEFINE_CHANNEL(8, DDRB, PORTB, PORTB0);  //Arduino pin 8
SOFTPWM_DEFINE_CHANNEL(9, DDRB, PORTB, PORTB1);  //Arduino pin 9
SOFTPWM_DEFINE_CHANNEL(10, DDRB, PORTB, PORTB2); //Arduino pin 10
SOFTPWM_DEFINE_CHANNEL(11, DDRB, PORTB, PORTB3); //Arduino pin 11
SOFTPWM_DEFINE_CHANNEL(12, DDRB, PORTB, PORTB4); //Arduino pin 12
SOFTPWM_DEFINE_CHANNEL(13, DDRB, PORTB, PORTB5); //Arduino pin 13
SOFTPWM_DEFINE_CHANNEL(14, DDRC, PORTC, PORTC0); //Arduino pin A0
SOFTPWM_DEFINE_CHANNEL(15, DDRC, PORTC, PORTC1); //Arduino pin A1
SOFTPWM_DEFINE_CHANNEL(16, DDRC, PORTC, PORTC2); //Arduino pin A2
SOFTPWM_DEFINE_CHANNEL(17, DDRC, PORTC, PORTC3); //Arduino pin A3
SOFTPWM_DEFINE_CHANNEL(18, DDRC, PORTC, PORTC4); //Arduino pin A4
SOFTPWM_DEFINE_CHANNEL(19, DDRC, PORTC, PORTC5); //Arduino pin A5

SOFTPWM_DEFINE_OBJECT_WITH_PWM_LEVELS(20, 100);
SOFTPWM_DEFINE_EXTERN_OBJECT_WITH_PWM_LEVELS(20, 100);

char inputString = "";
String serialInput = "";
bool stringComplete = false;
int pwm = 0;
void setup()
{
  Serial.setTimeout(100);
  serialInput.reserve(100);
  Palatis::SoftPWM.begin(240);
  Palatis::SoftPWM.printInterruptLoad();
  Serial.begin(9600);
}
void loop()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    serialInput += inChar;
    if (inChar == '\n')
    {
      stringComplete = true;
      serialInput = serialInput.substring(1, serialInput.length() - 1);
      pwm = serialInput.toInt();
    }
    else
    {
      if (serialInput.length() < 2)
        inputString = inChar;
    }
  }
  if (stringComplete)
  {
    switch (inputString)
    {
    case 116:
      PWM(RoyalBlue);
      break;

    case 97:
      PWM(Amber);
      break;

    case 105:
      PWM(IR);
      break;

    case 119:
      PWM(White);
      break;

    case 117:
      PWM(UV);
      break;

    case 114:
      PWM(Red);
      break;

    case 98:
      PWM(Blue);
      break;

    case 103:
      PWM(Green);
      break;

    case 108:
      FULL_LOOP();
      break;

    default:
      LOW_CHANNEL();
      break;
    }
    inputString = "";
    serialInput = "";
    stringComplete = false;
  }
}

static volatile uint8_t v = 0;

void PWM(uint8_t channel)
{
  unsigned long const WAIT = 1000000 / Palatis::SoftPWM.PWMlevels() * 2;
  unsigned long nextMicros = 0;
  Palatis::SoftPWM.set(channel, pwm);
  Serial.println("Done");
  //Palatis::SoftPWM.set(channel, 0);
}

void FULL_LOOP()
{
  for (uint8_t i = 12; i < Palatis::SoftPWM.size(); ++i)
  {
    unsigned long const WAIT = 1000000 / Palatis::SoftPWM.PWMlevels() / 8;
    unsigned long nextMicros = 0;
    for (int v = 0; v < Palatis::SoftPWM.PWMlevels() - 1; ++v)
    {
      while (micros() < nextMicros)
        ;
      nextMicros = micros() + WAIT;
      Palatis::SoftPWM.set(i, pwm);
    }
    Palatis::SoftPWM.set(i, 0);
  }
  Serial.println("Done");
}
void LOW_CHANNEL()
{
  for (uint8_t i = 12; i < Palatis::SoftPWM.size(); ++i)
  {
    Palatis::SoftPWM.set(i, 0);
  }
  Serial.println("Done");
}
